package com.pri.demospring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemospringApplication

fun main(args: Array<String>) {
	runApplication<DemospringApplication>(*args)
}
