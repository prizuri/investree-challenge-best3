package com.pri.demospring.service

import com.pri.demospring.entity.Book
import com.pri.demospring.repository.BookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookService @Autowired constructor(
    private val bookRepository: BookRepository
){
    fun getBooks(): List<Book>{
        return bookRepository.findAll()
    }

    fun addNewBook(book: Book): Book{
        return bookRepository.save(book)
    }

    fun updateBook(id: Int, book: Book): Book{
        val updateBook = bookRepository.findById(id).get()
        updateBook.author=book.author
        updateBook.isbn=book.isbn
        updateBook.year=book.year
        updateBook.title=book.title
        return bookRepository.save(updateBook)
    }

    fun deleteBook(id: Int): Int{
        bookRepository.deleteById(id)
        return id
    }
}